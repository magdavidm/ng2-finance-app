import { Injectable }    from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable }     from "rxjs/Observable";

import { QuoteChain } from "../models/quote-chain.model";

@Injectable()
export class QuoteService {
    response: string;
    tickerInput: string;
    trackedStocks: Array<QuoteChain> = [];
    constructor (public http: Http) {
    }
    callAjax(url, callback) {
        fetch(url)
        .then(function(response){
            return response.json();
        })
        .then(function(msgContent){
            callback(msgContent);
        });
    }

    private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : "Server error";
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}

