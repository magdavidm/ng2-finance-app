export const htmlTemplate = `
<h2>
    Quotes
</h2>

<div class="search-bar">
    <span><input [(ngModel)]="tickerInput" placeholder="(TSLA, APL, ...)" /></span>
    <span><button (click)="getTicker()">Get Quote</button></span>
</div>
<img src="img/arrow-chart-144.png" alt="gears" height="42" width="42">

<nav>
<a [routerLink]="['summary']">Summary</a>
<a [routerLink]="['news']">News</a>
<a [routerLink]="['options']">Options</a>
<a [routerLink]="['chart']">Chart</a>
</nav>
<router-outlet></router-outlet>
`;