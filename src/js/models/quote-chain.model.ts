export class QuoteChain {
    symbol: string;
    highValues: Array<number> = [];
    volumeChain: Array<number> = [];
    dates: Array<string> = [];
}