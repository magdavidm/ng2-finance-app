import { Component } from "@angular/core";
import { RouteParams } from "@angular/router-deprecated";

import { QuoteService } from "../../services/quote.service";
import { htmlTemplate } from "../../templates/quote/options.html";

@Component({
    selector: "quote-options",
    template: htmlTemplate
})

export class OptionsComponent {
    constructor(
        private quoteService: QuoteService
        ) { }
    tickerInput: string;
    trackedStocks = this.quoteService.trackedStocks;

    addUser() {
        let app = this;
    }
}