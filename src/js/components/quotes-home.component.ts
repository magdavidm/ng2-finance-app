import { Component } from "@angular/core";
import { RouteParams } from "@angular/router-deprecated";
import { ROUTER_DIRECTIVES } from "@angular/router";

import { QuoteService } from "../services/quote.service";
import { htmlTemplate } from "../templates/quotes-home.html";

import { SummaryComponent } from "../components/quote/summary.component";
import { NewsComponent } from "../components/quote/news.component";
import { OptionsComponent } from "../components/quote/options.component";

import { QuoteChain } from "../models/quote-chain.model";

@Component({
    directives: [ROUTER_DIRECTIVES],
    selector: "quotes-home",
    template: htmlTemplate
})

export class QuotesHomeComponent {
    constructor(
        private quoteService: QuoteService
        ) { }
    tickerInput: string;

    getTicker() {
        let app = this;
        app.quoteService.tickerInput = app.tickerInput;
        let urlBase = "/quote/";
        let url = urlBase + app.tickerInput;
        // console.log("sending GET for: " + url);
        app.quoteService.callAjax(url, function(msg) {
            // let msgAsJSON = JSON.parse(msg);
            console.log(typeof(msg));
            console.log(msg);
            let quoteChain = new QuoteChain();
            quoteChain.symbol = msg[0].symbol;
            for (let i = 0; i < msg.length; i++) {
                quoteChain.highValues.push(msg[i].high);
                quoteChain.dates.push(msg[i].date);
            }
            app.quoteService.trackedStocks.push(quoteChain);
            console.log(app.quoteService.trackedStocks.length);
        });
    }
}