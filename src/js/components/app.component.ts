import { Component } from "@angular/core";
import { ROUTER_DIRECTIVES } from "@angular/router";

import { QuotesHomeComponent } from "../components/quotes-home.component";
import { QuoteService } from "../services/quote.service";

@Component({
    directives: [ROUTER_DIRECTIVES],
    providers: [
        QuoteService,
    ],
    selector: "my-app",
    styleUrls: ["dist/css/component/app.component.css"],
    template: `
    <h1>{{title}}</h1>
    <nav>
    <a [routerLink]="['/']">Quotes Home</a>
    </nav>
    <router-outlet></router-outlet>
    `,
})
export class AppComponent {
    title = "Finance App";
}
