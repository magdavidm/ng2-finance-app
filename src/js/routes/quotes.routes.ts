import { RouterConfig }          from "@angular/router";

import { QuotesHomeComponent } from "../components/quotes-home.component";
import { SummaryComponent } from "../components/quote/summary.component";
import { NewsComponent } from "../components/quote/news.component";
import { OptionsComponent } from "../components/quote/options.component";

export const QuotesRoutes: RouterConfig = [
  {
    path: "quotes",
    component: QuotesHomeComponent,
    children: [
      { path: "",  component: SummaryComponent },
      { path: "summary",  component: SummaryComponent },
      { path: "news",     component: NewsComponent },
      { path: "options",  component: OptionsComponent },
    ]
  }
];