import { provideRouter, RouterConfig } from "@angular/router";

import { QuotesRoutes } from "./quotes.routes";

import { QuotesHomeComponent } from "../components/quotes-home.component";
import { SummaryComponent } from "../components/quote/summary.component";
import { NewsComponent } from "../components/quote/news.component";
import { OptionsComponent } from "../components/quote/options.component";
import { ChartComponent } from "../components/quote/chart.component";

export const routes: RouterConfig = [
  {
    // component: QuotesHomeComponent,
    path: "",
    redirectTo: "quotes",
  },
  {
    component: QuotesHomeComponent,
    path: "quotes",
    children: [
      { path: "", component: SummaryComponent },
      { path: "summary", component: SummaryComponent },
      { path: "news", component: NewsComponent },
      { path: "options", component: OptionsComponent },
      { path: "chart", component: ChartComponent },
    ],
  },
  {
    component: OptionsComponent,
    path: "test",
  },
  // QuotesRoutes
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes),
];
