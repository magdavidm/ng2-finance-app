let caches: any;
let fetch: any;
interface Window {
  clients: any;
  fetch: any;
  caches: any;
  skipWaiting: any;
};

this.addEventListener("install", e => {
  e.waitUntil(
    caches.open("ngswbasic").then(cache => {
      return cache.addAll([
        "/",
        "/service-worker-registration.js",
        "/loading.css",
        "/index.html",
        "/index.html?homescreen=1",
        "/?homescreen=1",
        "/img/arrow-chart-144.png",
        "/dist/css/component/app.component.css",
        "/dist/css/component/chart.component.css",
        "/dist/js/app.min.js",
        "/lib/js/vendors.min.js"
      ])
        .then(() => self.skipWaiting());
    })
  );
});

this.addEventListener("activate", event => {
  event.waitUntil(self.clients.claim());
});

this.addEventListener("fetch", function (event) {
  event.respondWith(
    caches.match(event.request).then(function (response) {
      return response || fetch(event.request);
    })
  );
});