//"use strict";
let System = require("systemjs");

let path = require('path');
var express = require("express");
var app = express();
var rootPath = path.normalize(__dirname);

var yahooFinance = require('yahoo-finance');

app.set("port", (process.env.PORT || 8080));

// for static files
app.use('', express.static(rootPath + "/public"));

app.get('/quote/:ticker', function (req, res) {
    let ticker = req.params.ticker;
    console.log("getting " + ticker);
    getStockInfo(ticker, function(msg){
      res.json(msg);
    })
});

// anything else, send the NG2 app
app.get("*", function(req, res) {
  console.log("sending application");
  res.sendFile(rootPath + "/public/index.html");
});

app.listen(app.get("port"), function() {
  console.log("Node app is running on port", app.get("port"));
});

function getStockInfo(SYMBOL, callback){
  let today = new Date();
  let newestDate = dateFormatForYF(today);
  let oldestDate = dateFormatForYF(new Date((today.getTime() - (14*86400*1000)))); //86400 seconds in 1 day
  // console.log(newestDate);
    yahooFinance.historical({
      symbol: SYMBOL,
      from: oldestDate,
      to: newestDate,
      period: 'd'
    }, function(err, quotes){
      // console.log(typeof(quotes));
      callback(quotes);
    });
}

function dateFormatForYF(someDate: Date){
  let year = someDate.getFullYear().toString();
  let month = (someDate.getMonth() > 9) ? (someDate.getMonth()+1).toString() : '0' + (someDate.getMonth()+1).toString();
  let date = (someDate.getDate() > 9) ? someDate.getDate().toString() : '0' + someDate.getDate().toString();
  return year + '-' + month + '-' + date;
}